FROM rust:1.63.0
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain nightly
RUN cargo +nightly install junitify --version=$VERSION
